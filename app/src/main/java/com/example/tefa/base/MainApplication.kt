package com.example.tefa.base

import android.app.Application
import com.example.tefa.BuildConfig
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class MainApplication : Application() {
    companion object{
        var BASE_URL = BuildConfig.BASE_URL
    }
}