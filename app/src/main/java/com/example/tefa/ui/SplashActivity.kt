package com.example.tefa.ui

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.example.tefa.data.model.UserItem
import com.example.tefa.databinding.ActivitySplashBinding
import com.example.tefa.ui.viewmodel.MainViewModel
import com.example.tefa.utils.IntentKey
import com.kodekita.toaster.ToasterMessage
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

@AndroidEntryPoint
@SuppressLint("CustomSplashScreen")
class SplashActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashBinding
    private lateinit var mainViewModel: MainViewModel
    private var dataList: List<UserItem>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivitySplashBinding.inflate(layoutInflater)
        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        setContentView(binding.root)
        initIntent()
        initView()
        initObserver()
        initAction()
    }

    private fun initIntent() {
        // Get all your intent here
        mainViewModel.getUserList()
    }

    private fun initView() {
        ToasterMessage.toaster(this, "Testing libs")
        // Set your default UI Logic
    }

    private fun initObserver() {
        // Init LiveData Observer here
        mainViewModel.userList.observe(this) {
            dataList = it
        }

        mainViewModel.isLoading.observe(this) {
            Log.d(TAG, it.toString())
        }
    }

    @OptIn(DelicateCoroutinesApi::class)
    private fun initAction() {
        // Set interaction between user and UI here
        GlobalScope.launch {
            delay(3000L)
            val intent = Intent(this@SplashActivity, MainActivity::class.java)
            intent.putExtra(IntentKey.KEY_USER, dataList?.get(0)?.avatar)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            finish()
        }
    }

    companion object {
        private const val TAG = "SplashActivity"
    }
}